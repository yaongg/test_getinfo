/**
 * File       : promotion.js
 * Author     : m-sync (baeyoungseop)
 */

// HACK: vh property reset
var vh = window.innerHeight * 0.01;
document.documentElement.style.setProperty('--vh', vh + 'px');

$(window).resize(function() {
  vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty('--vh', vh + 'px');
});

$(document).ready(function() {
  var mainVisual = (function() {
    var state = {
      isPlay: false,
    };
    var element = {
      text: document.querySelector('.main_visual_text'), // 메인텍스트
      thumb: document.querySelector('.main_video_thumb'), // 비디오썸네일
      videoWrap: document.querySelector('.video_wrap'),
      video: document.querySelector('.video_wrap video'), // 영상
      closeButton: document.querySelector('.btn_close'), // 닫기버튼
    };
    
    // 영상재생 종료시
    element.video.onended = function() {
      mainVisual.toggleElementAnimation();
      gtag('event', 'onend', {
        'event_category': 'video',
        'event_label': '영상 재생 완료 횟수',
        'value': 1,
      });
    };

    return {
      toggleElementAnimation: function() {
        if (!state.isPlay) {
          TweenMax.to(element.text, 1, {
            ease: Power3.easeOut,
            // x: -100,
            autoAlpha: 0,
          });
          TweenMax.to(element.thumb, 1, {
            ease: Power3.easeOut,
            // marginRight: -100,
            autoAlpha: 0,
          });
          TweenMax.to(element.videoWrap, 1, {
            ease: Power3.easeOut,
            autoAlpha: 1,
            delay: 0.4,
            onComplete: function() {
              element.video.currentTime = 0;
              element.video.play();
            },
          });
          state.isPlay = true;
        } else {
          TweenMax.to(element.text, 1, {
            ease: Power3.easeOut,
            // x: 0,
            autoAlpha: 1,
            delay: 0.4,
          });
          TweenMax.to(element.thumb, 1, {
            ease: Power3.easeOut,
            // marginRight: 0,
            autoAlpha: 1,
            delay: 0.4,
          });
          TweenMax.to(element.videoWrap, 1, {
            ease: Power3.easeOut,
            autoAlpha: 0,
            onStart: function() {
              element.video.pause();
            },
          });
          state.isPlay = false;
        }
      },
      element: element,
    };
  })();

  // 영상 재생 버튼
  $('.btn_play').click(function() {
    mainVisual.toggleElementAnimation();
    gtag('event', 'onstart', {
      'event_category': 'video',
      'event_label': '영상 재생 시작 횟수',
      'value': 1,
    });
  });

  // 영상 닫기 버튼
  $(mainVisual.element.closeButton).click(function() {
    mainVisual.toggleElementAnimation()
  });

  // 19금 오버레이
  $('#yes').click(function(e) {
    e.preventDefault();
    $('.over-19-popup').fadeOut(400);
  });
  $('#no').click(function(e) {
    e.preventDefault();
    $('.over-19-popup h5').text(
      '본 사이트는 대한민국에 거주하는 만 19세 이상의 성인흡연자에게만 열람 및 이용이 허용됩니다.'
    );
    $('.over-19-popup .popup_inner > p').hide();
    $('.over-19-popup .list_btn_wrap').hide();
  });

  // 스크롤
  $('.scr_circle .circle, .inner_circle').click(function() {
    $('html, body').animate({
      scrollTop: (function() {
        return $('.contact').offset().top;
      })()
    }, 500, 'easeOutExpo');
  });

  //input 클릭 시 팝업 display 값 변경
  // $('.agree input').on('click',function(){
  //   // $('.checkbox_popup').css('display','flex');
  //   // popupContentDisplay($(this).parent('li').index() - 1);
  // });

  //팝업 li 항목 클릭시 active
  // $('.checkbox_popup .pop_check li').on('click',function(){
  //   // $('.checkbox_popup .pop_check li').removeClass('active');
  //   // $(this).addClass('active');
  //   // popupContentDisplay($(this).index());
  // });

  // $('.checkbox_popup .pop_check li').eq(0).on('click',function(){
  //   $('.checkbox_popup .caption').hide();
  //   $('.checkbox_popup .caption').eq(0).show();
  // });
  // $('.checkbox_popup .pop_check li').eq(1).on('click',function(){
  //   $('.checkbox_popup .caption').hide();
  //   $('.checkbox_popup .caption').eq(1).show();
  // });
  // $('.checkbox_popup .pop_check li').eq(2).on('click',function(){
  //   $('.checkbox_popup .caption').hide();
  //   $('.checkbox_popup .caption').eq(2).show();
  // })

  function popupContentDisplay(index) {
    if ( index < 0 ) index = 0;
    
    $('.checkbox_popup .pop_check li').eq(index).addClass('active').siblings().removeClass('active');
    $('.checkbox_popup .caption').hide().eq(index).show();

    var popChk = $('.pop_check input[type=checkbox]').length; //전체 동의 input 1개 빼기  
    var popChked = $('.pop_check input[type=checkbox]:checked').length;
    //팝업 체크 전체 동의 연동
    if( popChk === popChked){
      $('.agree #chk_all').prop('checked',true)
    }else{
      $('.agree #chk_all').prop('checked',false);
    }
  }

  //팝업 X 버튼 누르면 팝업 닫기
  $('.checkbox_popup .popup_title .close').on('click',function(){
    $('.checkbox_popup').hide();
  });

  //본문 전체동의
  $('.agree #chk_all').on('click',function(){
    if($('.agree #chk_all').prop('checked')){
      $('.agree input[type=checkbox]').prop('checked',true);
      $('.checkbox_popup .pop_check input[type=checkbox]').prop('checked',true);
    }else{
      $('.agree input[type=checkbox]').prop('checked',false);
      $('.checkbox_popup .pop_check input[type=checkbox]').prop('checked',false);
    }
  });

  //팝업 체크 박스 상태 변경 시
  $('.checkbox_popup .pop_check input[type=checkbox]').on('change',function(){
    var status = $(this).prop('checked');
    var indexNo = $(this).parent('li').index();

    //팝업 체크 화면 체크 연동
    $('.agree li').eq( indexNo + 1 ).find('input[type=checkbox]').prop('checked', status);

    
    // console.log($(this).parent('li').index(), $(this).prop('checked'));

    popupContentDisplay(indexNo);
    
  });
  
  //본문 체크 박스 상태 변경 시
  $('.agree input[type=checkbox]').on('change',function(){
    var status = $(this).prop('checked');
    var indexNo = $(this).parent('li').index();
    $('.checkbox_popup .pop_check li').eq(indexNo - 1).find('input[type=checkbox]').prop('checked',status);

    $('.checkbox_popup').css('display','flex');
    popupContentDisplay($(this).parent('li').index() - 1);
  });

  //쿠폰 클릭 시
  $('.btn_coupon_wrap button').on('click',function(){
    var customerName = $("#prm_username").val();
    var customerPhonenumber = $("#prm_tel").val();
    // var customerName = $("#prm_username").val('test');
    // var customerPhonenumber = $("#prm_tel").val('01032365702');

    // 성명 미입력시
    if(customerName == ''){
      alert('성명 입력 후 진행 부탁드립니다.');
      return false;
    }

    // 연락처 미입력시
    if(customerPhonenumber == ''){
      alert('연락처 입력 후 진행 부탁드립니다.');
      return false;
    }
    // 20세 이상 성인 미동의시
    if(!$("#chk_adult").is(":checked")){
      alert('20세 이상 성인 동의 시 진행 가능하며, \n' +'성인이 아닐 경우 진행이 불가합니다.');
      return false;
    }

    // 필수항목 미동의시
    if(!$("#chk_tc1").is(":checked")){
      alert('필수 항목은 반드시 동의해야 진행 가능합니다.');
      return false;
    }

    var agreeCount = 0;
    var checkForm = ['chk_adult', 'chk_tc1', 'chk_tc2', 'chk_tc3', 'chk_tc4'];
    for ( var i in checkForm) {
      if ( $("#"+ checkForm[i]).is(":checked") ) {
        agreeCount++;
        if(i != 0){
          $("#chk_tc"+i).val(1);
        }
      }else{
        $("#chk_tc"+i).val(0);
      }
    }
    if( agreeCount < 5){
      alert('전체 동의하시는 경우에만 아이코스 대여 관련 안내 내용 수신 및 특별 구매 할인 쿠폰 코드 발급이 가능합니다.');
      return false;
    }

    if ( agreeCount === 5 ) {
      console.log('Form 전송')

      var obj = {'customerName' :customerName ,'customerPhonenumber': customerPhonenumber, 'tc1':$("#chk_tc1").val(), 'tc2' :$("#chk_tc2").val(), 'tc3' :$("#chk_tc3").val(), 'tc4' :$("#chk_tc4").val()};

      // var ccBridgeUrl = "http://localhost:8080/api/ccBridging";
      var ccBridgeUrl = "https://tryiqosdev.iqossvc.com/api/ccBridging";
      // var ccBridgeUrl = "https://tryiqos.kr/api/ccBridging";

      $.ajax({
        url : ccBridgeUrl,
        type : "POST",
        // data : "customerName="+customerName +"&customerPhonenumber="+customerPhonenumber+"&tc1="+$("#chk_tc1").val()+"&tc2="+$("#chk_tc2").val()+"&tc3="+$("#chk_tc3").val(),
        data : {'customerName' :customerName ,'customerPhonenumber': customerPhonenumber, 'tc1':$("#chk_tc1").val(), 'tc2' :$("#chk_tc2").val(), 'tc3' :$("#chk_tc3").val(), 'tc4' :$("#chk_tc4").val()},
        dataType : "text",
        // jsonp : "callback",
        success :function (data) {
          if (data == 'SUCCESS_COUPON') {
            alert('특별 구매 할인 쿠폰이 발급되었습니다.\n' + '고객님 휴대폰에서 문자 확인하여주세요.');
          }else if(data == 'SUCCESS'){
            // 없는 경우
          }else if(data=='ERROR_DUPLICATE'){
            alert('이미 쿠폰을 발급 받으셨습니다.');
          }else if(data == 'ERROR_SEND_SMS'){
            alert('문자 발송을 실패하였습니다.\n'+'잠시후 다시 시도해주세요.');
          }else if (data == 'ERROR_NOT_EXIST_COUPON'){
            alert('발급 가능한 쿠폰이 없습니다.')
          }else{
            alert('쿠폰 발급 중 오류가 발생하였습니다.\n'+ '잠시후 다시 시도해주세요.');
          }
        },
        error : function (xhr) {
          alert('잠시후 다시 시도해주세요.');
        }
      });
    }
    // var allChklen = $('input[type=checkbox]:checked').length;
    // console.log(allChklen)
    // if(allChklen < 5){
    //   alert('전체 동의하시는 경우에만 아이코스 대여 관련 안내 내용 수신 및 특별 구매 할인 쿠폰 코드 발급이 가능합니다.');
    // }
  });



  // 전화번호 유효성 체크
  $(".input_phone").on('keydown', function(e){
    // 숫자만 입력받기
    var trans_num = $(this).val().replace(/-/gi,'');
    var k = e.keyCode;

    if(trans_num.length >= 11 && ((k >= 48 && k <=126) || (k >= 12592 && k <= 12687 || k==32 || k==229 || (k>=45032 && k<=55203)) ))
    {
      e.preventDefault();
    }
  }).on('blur', function(){ // 포커스를 잃었을때 실행합니다.
    if($(this).val() == '') return;

    // 기존 번호에서 - 를 삭제합니다.
    var trans_num = $(this).val().replace(/-/gi,'');

    // 입력값이 있을때만 실행합니다.
    if(trans_num != null && trans_num != '')
    {
      // 총 핸드폰 자리수는 11글자이거나, 10자여야 합니다.
      if(trans_num.length==11 || trans_num.length==10) {
        // 유효성 체크
        var regExp_ctn = /^(01[016789]{1}|02|0[3-9]{1}[0-9]{1})([0-9]{3,4})([0-9]{4})$/;
        if(regExp_ctn.test(trans_num)){
          // 유효성 체크에 성공하면 값을 바꿔줍니다.
          trans_num = trans_num.replace(/^(01[016789]{1}|02|0[3-9]{1}[0-9]{1})-?([0-9]{3,4})-?([0-9]{4})$/, "$1$2$3");
          $(this).val(trans_num);
        }else {
          alert("유효하지 않은 전화번호 입니다.");
          $(this).val("");
          $(this).focus();
        }
      } else {
        alert("유효하지 않은 전화번호 입니다.");
        $(this).val("");
        $(this).focus();
      }
    }
  });


});
